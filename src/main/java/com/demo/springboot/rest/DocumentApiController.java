package com.demo.springboot.rest;

import com.demo.springboot.dto.ListOfFilmsDto;
import com.demo.springboot.service.ServiceImpl;
import com.demo.springboot.service.ServiceImple;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@RestController
public class DocumentApiController {
    @Autowired
    ServiceImpl serviceImpl;

    private static final Logger LOGGER = LoggerFactory.getLogger(DocumentApiController.class);
    
    @CrossOrigin
    @GetMapping(value="/movies")
    public ResponseEntity<ListOfFilmsDto> getListOfFilms(){

        return new ResponseEntity<>(serviceImpl.ShowFilms(),HttpStatus.OK);
    }
    

}
