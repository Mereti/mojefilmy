package com.demo.springboot.dto;

import java.util.ArrayList;
import java.util.List;

public class ListOfFilmsDto {
    private List<FilmDto> movies = new ArrayList<>();

    public ListOfFilmsDto(List<FilmDto> movies) {
        this.movies = movies;
    }
    public ListOfFilmsDto(){

    }

    public List<FilmDto> getMovies() {
        return movies;
    }

    public void setMovies(List<FilmDto> movies) {
        this.movies = movies;
    }

    @Override
    public String toString() {
        return "ListOfFilmsDto{" +
                "movies=" + movies +
                '}';
    }
}
