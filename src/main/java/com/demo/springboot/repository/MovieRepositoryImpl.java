package com.demo.springboot.repository;

import com.demo.springboot.dto.FilmDto;
import com.demo.springboot.dto.ListOfFilmsDto;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Repository
public class MovieRepositoryImpl implements MovieRepository{

    private ListOfFilmsDto listOfFilmsDto;

    @Override
    public ListOfFilmsDto GetListOfFilms() {
        List<FilmDto> movieList = new ArrayList<>(Arrays.asList(
                new FilmDto(
                        1,
                        "Piraci z krzemowej doliny",
                        1999,
                        " https://fwcdn.pl/fpo/30/02/33002/6988507.6.jpg"
                ),
                new FilmDto(
                        2,
                        "Ja, robot",
                        2004,
                        "https://fwcdn.pl/fpo/54/92/95492/7521206.6.jpg"
                ),
                new FilmDto(
                        3,
                        "Kod nieśmiertelności",
                        2011,
                        "https://fwcdn.pl/fpo/89/67/418967/7370853.6.jpg"
                ),
                new FilmDto(
                        4,
                        "Ex Machina",
                        2015,
                        "https://fwcdn.pl/fpo/64/19/686419/7688121.6.jpg"
                )
        ));
        this.listOfFilmsDto = new ListOfFilmsDto(movieList);

        return listOfFilmsDto;
    }
}
