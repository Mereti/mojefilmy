package com.demo.springboot.repository;

import com.demo.springboot.dto.ListOfFilmsDto;

public interface MovieRepository {
    ListOfFilmsDto GetListOfFilms( );
}
