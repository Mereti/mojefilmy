package com.demo.springboot.service;

import com.demo.springboot.dto.ListOfFilmsDto;
import com.demo.springboot.repository.MovieRepositoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ServiceImpl implements ServiceImple {
    @Autowired
    MovieRepositoryImpl movieRepository;

    @Override
    public ListOfFilmsDto ShowFilms() {
        return movieRepository.GetListOfFilms();
    }
}
