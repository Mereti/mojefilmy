package com.demo.springboot.service;


import com.demo.springboot.dto.FilmDto;
import com.demo.springboot.dto.ListOfFilmsDto;

public interface ServiceImple {
    ListOfFilmsDto ShowFilms();
}
